/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

// Chakra imports
import {
  Flex,
  useColorModeValue,
  FormControl,
  Button,
  Text,
  Image,
  Select,
  SimpleGrid,
  Input,
} from "@chakra-ui/react";
import Card from "components/card/Card.js";
import InputField from "components/fields/InputField";
import TextField from "components/fields/TextField";
import MiniCalendar from "components/calendar/MiniCalendar";
import Dropzone from "./Dropzone";
// Custom components
import {
  getTimeFromDate,
  updateDateTime,
  normalizeDate,
  timestampToTime,
  timeToTimeStamp,
} from "utils/datetime";
import { DeleteButton } from "components/buttons/DeleteButton";
import Label from "../../../../../components/fields/Label";
import { Spinner } from "components/spinner";

export default function FormCard({ event, path, setShowForm, user }) {
  let history = useHistory();

  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const [currentEvent, setCurrentEvent] = useState(event);
  const [name, setName] = useState(event?.name);
  const [startTimeTime, setStartTimeTime] = useState("");
  const [endTimeTime, setEndTimeTime] = useState("");
  const [startTime, setStartTime] = useState(
    event?.start_time ? event.start_time : new Date()
  );
  const [endTime, setEndTime] = useState(
    event?.end_time ? event.end_time : new Date()
  );
  const [location, setLocation] = useState(
    event?.location ? event.location : ""
  );
  const [eventType, setEventType] = useState(
    event?.event_type ? event?.event_type : ""
  );
  const [cover, setCover] = useState(event?.cover);
  const [newCover, setNewCover] = useState(null);
  const [description, setDescription] = useState(
    event?.description ? event.description : ""
  );
  const [tags, setTags] = useState([]);
  const [status, setStatus] = useState(
    event?.status ? event?.status : "pending"
  );
  const [errorMsg, setErrorMsg] = useState("");
  const [inProgress, setInProgress] = useState(false);

  const showError = () => {
    setErrorMsg("You can't delete the Event");
    setInterval(() => {
      setErrorMsg("");
    }, 2000);
  };

  const handleDelete = async () => {
    if (inProgress) return;
    if (!user) return;
    setInProgress(true);
    const response = await fetch(path, {
      method: "DELETE",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
    });
    const json = await response.json();
    setInProgress(false);
    if (response.ok) {
      console.log("DELETE EVENT RES: ", json);
      history.goBack();
    } else {
      showError();
    }
  };

  const changeTime = async (e, source) => {
    e.preventDefault();
    let val = e.target.value;
    source == "start" ? setStartTimeTime(val) : setEndTimeTime(val);
  };

  const getTimeValue = async () => {
    let start = await getTimeFromDate(startTime);
    console.log("start: ", startTime);
    let start_formated = await timestampToTime(start);
    setStartTimeTime(start_formated);
    let end = getTimeFromDate(endTime);
    let end_formated = await timestampToTime(end);
    setEndTimeTime(end_formated);
  };

  useEffect(() => {
    if (!startTimeTime && !endTimeTime) getTimeValue();
  }, [startTime, endTime]);

  useEffect(() => {
    localStorage.setItem("event_route", "edit");
    if (event) setCurrentEvent(event);
    console.log("EVENT: ", event);
  }, [event]);

  useEffect(() => {
    console.log("PROGRESS: ", inProgress);
  }, [inProgress]);

  const uploadImage = async (path) => {
    let full_path = path + "/cover";
    const formData = new FormData();
    formData.append("file", newCover);

    try {
      const response = await fetch(full_path, {
        method: "POST",
        headers: {
          Authorization: user,
        },
        body: formData,
      });
      if (response) console.log("UPLOAD RES: ", response);
      if (response.ok) window.location.reload(false);
    } catch (e) {
      throw new Error(e);
    }
    setInProgress(false);
  };

  const handleSave = async () => {
    if (inProgress) return;
    setErrorMsg("");
    if (!user) return;
    if (!cover && !newCover) {
      setErrorMsg("Can't save event without cover");
      return;
    }
    setInProgress(true);

    let start_date_and_time = await updateDateTime(startTime, startTimeTime);
    let end_date_and_time = await updateDateTime(endTime, endTimeTime);

    const body = {
      event: {
        name: name,
        location: location,
        description: description,
        start_time: new Date(start_date_and_time),
        end_time: new Date(end_date_and_time),
        event_type: eventType,
        status: status,
      },
    };
    console.log("CREATE EVENT BODY: ", JSON.stringify(body));

    const response = await fetch(path, {
      method: event ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: user,
      },
      body: JSON.stringify(body),
    });
    const json = await response.json();
    console.log("JSON: ", JSON.stringify(json));

    if (!response.ok) {
      setErrorMsg(json.error.message);
      setInProgress(false);
    }
    if (response.ok) {
      if (!newCover) {
        setCurrentEvent(json.event);
        console.log("RES: ", JSON.stringify(json));
        setInProgress(false);
        window.location.reload(false);
      } else if (newCover)
        uploadImage(event ? path : path + "/" + json.event.id);
    }
    return;
  };

  const handleCancel = () => {
    if (!event) {
      setShowForm(false);
      return;
    }
    setName(event?.name);
    setStartTime(event?.start_time ? event.start_time : new Date());
    setEndTime(event?.end_time ? event.end_time : new Date());
    setLocation(event?.location ? event.location : "");
    setEventType(event?.event_type ? event?.event_type : "");
    setCover(event?.cover);
    setNewCover(null);
    setDescription(event?.description ? event.description : "");
    setTags([]);
  };

  // Chakra Color Mode
  return (
    <Flex
      w="100%"
      justifyContent={"center"}
      direction={"column"}
      alignItems={"center"}
    >
      <FormControl w="60%" minW="300px" mt={"20px"}>
        <Card mb="20px">
          <InputField
            mb="10px"
            me="30px"
            id="name"
            label="Event name"
            // placeholder="The best event"
            required={true}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <InputField
            mb="10px"
            id="location"
            label="Location"
            // placeholder="Dizengoff 1, Tel Aviv"
            required={true}
            value={location}
            onChange={(e) => setLocation(e.target.value)}
          />
          <Label label={"Event type"} required={true} />
          <Select
            onChange={(e) => setEventType(e.target.value)}
            value={eventType}
            fontSize="sm"
            id="edit_product"
            variant="main"
            h="44px"
            w="20%"
            minW="200px"
            mb={"8px"}
            maxh="44px"
            me={{ base: "10px", md: "20px" }}
          >
            <option disabled={true} value="">
              Select
            </option>
            <option value="members">Members Only</option>
            <option value="free">Public Event</option>
            <option value="manual">Private Event</option>
          </Select>
          <Label label={"Cover"} required={true} />
          {cover && !newCover ? (
            <Flex
              mb={{ base: "20px", "2xl": "20px" }}
              justifyContent={"center"}
            >
              <Image
                src={cover}
                w={{ base: "50%", "3xl": "50%" }}
                h={{ base: "50%", "3xl": "50%" }}
                borderRadius="20px"
              />
            </Flex>
          ) : null}
          <Dropzone
            action={setNewCover}
            instructions={
              "Please make sure to upload *.jpeg and *.png Image 1:1 (or size 800x800 px)"
            }
          />
          <Label label={"Description"} required={true} />
          <TextField
            onChange={(e) => setDescription(e.target.value)}
            value={description}
            id="description"
            h="100px"
            placeholder="Tell something about yourself in 150 characters!"
          />
          {/* <TimePicker
            label={"Start time"}
            action={setStartTime}
            value={startTime}
          />
          <TimePicker label={"End time"} action={setEndTime} value={endTime} /> */}

          <SimpleGrid columns={{ base: 1, md: 1, xl: 2 }} gap="20px">
            <Flex
              w={{ base: "100%", "3xl": "100%" }}
              h={{ base: "100%", "3xl": "100%" }}
              direction={{ base: "column" }}
              alignItems={"center"}
            >
              <Text
                fontSize="sm"
                color={textColorPrimary}
                fontWeight="bold"
                marginInlineStart={"10px"}
                mb={"8px"}
              >
                Start time
              </Text>
              <input
                id="start-time"
                type="time"
                name="start-time"
                value={startTimeTime}
                onInput={(e) => changeTime(e, "start")}
              />
              <MiniCalendar
                selectRange={false}
                new_value={startTime}
                action={setStartTime}
              />
            </Flex>
            <Flex
              w={{ base: "100%", "3xl": "100%" }}
              h={{ base: "100%", "3xl": "100%" }}
              direction={{ base: "column" }}
              alignItems={"center"}
            >
              <Text
                fontSize="sm"
                color={textColorPrimary}
                fontWeight="bold"
                marginInlineStart={"10px"}
                mb={"8px"}
              >
                End time
              </Text>
              <input
                id="end-time"
                type="time"
                name="end-time"
                value={endTimeTime}
                onInput={(e) => changeTime(e, "end")}
              />
              <MiniCalendar
                selectRange={false}
                new_value={endTime}
                action={setEndTime}
              />
            </Flex>
          </SimpleGrid>
          <Text color="red">{errorMsg}</Text>
          {inProgress ? (
            <Spinner />
          ) : (
            <Flex
              direction={{ base: "column-reverse", md: "row" }}
              justifyContent={"center"}
              alignItems={"center"}
              mx="10px"
              mt="40px"
            >
              <DeleteButton
                action={() => {
                  handleDelete();
                }}
                isPassive={true}
              />
              <Button
                onClick={handleCancel}
                variant={"outline"}
                minW={"100px"}
                maxW={"100px"}
                fontSize="sm"
                size="md"
                fontWeight="500"
                borderRadius="70px"
                mx="5px"
              >
                Cancel
              </Button>
              <Button
                onClick={() => {
                  handleSave();
                }}
                variant={"brand"}
                minW={"100px"}
                maxW={"100px"}
                fontSize="sm"
                size="md"
                fontWeight="500"
                borderRadius="70px"
                mx="5px"
              >
                Save
              </Button>
            </Flex>
          )}
        </Card>
      </FormControl>
    </Flex>
  );
}
